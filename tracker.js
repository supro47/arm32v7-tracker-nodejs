var Server = require('bittorrent-tracker').Server

var server = new Server({
  http: true,
  stats: true,
  udp: true,
  ws: true
})

server.on('error', function (err) {
  console.error('ERROR: ' + err.message)
})
server.on('warning', function (err) {
  console.log('WARNING: ' + err.message)
  console.log(Object.keys(server.torrents))
})
server.on('update', function (addr) {
  console.log('update: ' + addr)
})
server.on('complete', function (addr) {
  console.log('complete: ' + addr)
})
server.on('start', function (addr) {
  console.log('start: ' + addr)
})
server.on('stop', function (addr) {
  console.log('stop: ' + addr)
})

server.listen(8000)